= Glossary

[options="header",cols="1,1"]
|===
| Name | Description

| Ligature
| combining different letters to one; e.g. combining '\->' to '->'

|===

