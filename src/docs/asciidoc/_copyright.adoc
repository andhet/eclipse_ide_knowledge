= Copyright

Copyright © 2021 Andreas Hetzel.
Except where noted, the content is licensed under a Creative Commons Attribution-ShareAlike 4.0 International Public License (https://creativecommons.org/licenses/by-sa/4.0/[CC BY_SA 4.0]).
